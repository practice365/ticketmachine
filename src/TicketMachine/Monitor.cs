﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketMachine
{
    /// <summary>
    /// モニター
    /// </summary>
    public static class Monitor
    {
        /// <summary>
        /// 入力を受け付ける
        /// </summary>
        /// <returns>入力文字列</returns>
        public static string GetInput()
        {
            return Console.ReadLine();
        }

        /// <summary>
        /// 商品選択画面 表示
        /// </summary>
        /// <param name="menus">メニュー</param>
        public static void DisplayChoiseMessage(string categoryName, List<Menu> menus)
        {
            Console.WriteLine($"--{categoryName}を選択してください--");
            DisplayMenuNames(menus);
        }

        /// <summary>
        /// 選択された商品名を表示
        /// </summary>
        /// <param name="menus">メニュー</param>
        public static void DisplayChosenMenuNames(List<Menu> menus)
        {
            Console.WriteLine("--選択商品--");
            DisplayMenuNames(menus);
        }

        /// <summary>
        /// 合計金額出力
        /// </summary>
        /// <param name="price">合計金額</param>
        public static void DisplayTotalPrice(int price)
        {
            Console.WriteLine("--合計金額--");
            Console.WriteLine($"{price}円");
        }

        /// <summary>
        /// 「選択肢の中から選んでください」を表示
        /// </summary>
        public static void DisplayRetryMessage()
        {
            Console.WriteLine("--選択肢の中から選んでください--");
        }

        /// <summary>
        /// カテゴリー内のメニューを注文するか確認するメッセージを表示
        /// </summary>
        /// <param name="category">カテゴリー</param>
        /// <returns>次の注文に進む場合はtrue</returns>
        public static bool AcceptsSkipOrder(Category category)
        {
            Console.WriteLine($"--{category.Name}を注文しますか？--");
            Console.WriteLine("注文する(y)、注文しない(n)");
            return AcceptsNo();
        }

        /// <summary>
        /// 注文の継続か清算かの確認メッセージを表示
        /// </summary>
        /// <returns>清算ならtrue、注文継続ならfalse</returns>
        public static bool AcceptsEndOfOrder()
        {
            Console.WriteLine("--注文を継続しますか？--");
            Console.WriteLine("注文を継続(y)、清算(n)");
            return AcceptsNo();
        }

        private static void DisplayMenuNames(List<Menu> menus)
        {
            var names = menus.Select(x => x.Name).ToList();
            foreach (var name in names)
            {
                Console.WriteLine(name);
            }
        }

        private static bool AcceptsYes()
        {
            while (true)
            {
                var input = Console.ReadLine();
                if (input.Equals("y"))
                {
                    return true;
                }
                else if (input.Equals("n"))
                {
                    return false;
                }
                else
                {
                    DisplayRetryMessage();
                }
            }
        }

        private static bool AcceptsNo()
        {
            return !AcceptsYes();
        }
    }
}