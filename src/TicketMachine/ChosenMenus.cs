﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketMachine
{
    /// <summary>
    /// 選択されたメニュー
    /// </summary>
    public class ChosenMenus
    {
        private const int DiscountAmount = 50;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ChosenMenus()
        {
            this.Menus = new List<Menu>();
        }

        /// <summary>
        /// メニュー
        /// </summary>
        public List<Menu> Menus { get; private set; }

        /// <summary>
        /// メニューを追加
        /// </summary>
        /// <param name="menu">メニュー</param>
        public void Add(Menu menu)
        {
            this.Menus.Add(menu);
        }

        /// <summary>
        /// 合計金額を取得する
        /// </summary>
        /// <returns>合計金額</returns>
        public int GetTotalPrice()
        {
            var totalPrice = this.Menus.Select(x => x.Price).Sum();
            return this.IsDiscount() ? totalPrice - DiscountAmount : totalPrice;
        }

        private bool IsDiscount()
        {
            // 選択されたメインメニュー、サイドメニュー1、サイドメニュー2それぞれに対して、
            // 割引対象が少なくとも1つ含まれる場合に割引を適用する
            var isDiscount1 = this.Menus.Any(x => x.N == 1 && x.S == 1);
            var isDiscount2 = this.Menus.Any(x => x.N == 2 && x.S == 1);
            var isDiscount3 = this.Menus.Any(x => x.N == 3 && x.S == 1);

            return isDiscount1 && isDiscount2 && isDiscount3;
        }
    }
}