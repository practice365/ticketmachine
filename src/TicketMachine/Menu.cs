﻿namespace TicketMachine
{
    /// <summary>
    /// メニュー
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="name">メニュー名</param>
        /// <param name="n">メニュー分類</param>
        /// <param name="s">セット割引分類</param>
        /// <param name="o">オプション分類</param>
        /// <param name="price">値段</param>
        public Menu(string name, int n, int s, int o, int price)
        {
            this.Name = name;
            this.N = n;
            this.S = s;
            this.O = o;
            this.Price = price;
        }

        /// <summary>
        /// メニュー名
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// メニュー分類
        /// </summary>
        public int N { get; }

        /// <summary>
        /// セット割引分類
        /// </summary>
        public int S { get; }

        /// <summary>
        /// オプション分類
        /// </summary>
        public int O { get; }

        /// <summary>
        /// 値段
        /// </summary>
        public int Price { get; }
    }
}