﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketMachine
{
    /// <summary>
    /// 商品カテゴリー
    /// </summary>
    public class Category
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="id">カテゴリーID</param>
        /// <param name="name">カテゴリー名</param>
        public Category(int id, string name)
        {
            this.Id = id;
            this.Name = name;
            this.Menus = new List<Menu>();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="id">カテゴリーID</param>
        /// <param name="name">カテゴリー名</param>
        /// <param name="menus">カテゴリーに含まれるメニュー</param>
        public Category(int id, string name, List<Menu> menus)
        {
            this.Id = id;
            this.Name = name;
            this.Menus = menus;
        }

        /// <summary>
        /// カテゴリーID
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// カテゴリー名
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// メニュー
        /// </summary>
        public List<Menu> Menus { get; private set; }

        /// <summary>
        /// メニュー追加
        /// </summary>
        /// <param name="menu">メニュー</param>
        public void Add(Menu menu)
        {
            this.Menus.Add(menu);
        }

        /// <summary>
        /// メニュー選択
        /// </summary>
        /// <returns>選択されたメニュー</returns>
        public Menu ChooseMenu()
        {
            Monitor.DisplayChoiseMessage(this.Name, this.Menus);
            while (true)
            {
                var input = Monitor.GetInput();
                var chosenMenu = this.Menus.Where(x => x.Name.Equals(input)).FirstOrDefault();
                if (chosenMenu != null)
                {
                    return chosenMenu;
                }
                else
                {
                    Monitor.DisplayRetryMessage();
                }
            }
        }
    }
}