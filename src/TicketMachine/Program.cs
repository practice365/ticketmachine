﻿using System;

namespace TicketMachine
{
    /// <summary>
    /// Program
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        private static void Main(string[] args)
        {
            var ticketMachine = new TicketMachine();
            ticketMachine.UseMachine();
        }
    }
}