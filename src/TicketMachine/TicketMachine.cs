﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicketMachine
{
    /// <summary>
    /// 券売機クラス
    /// </summary>
    public class TicketMachine
    {
        private Category mainMenu = new Category(1, "メインメニュー");
        private Category sideMenu1 = new Category(2, "サイドメニュー1");
        private Category sideMenu2 = new Category(3, "サイドメニュー2");
        private Category optionMenu = new Category(4, "オプション");
        private ChosenMenus chosenMenus = new ChosenMenus();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TicketMachine()
        {
            this.mainMenu.Add(new Menu("牛丼", 1, 1, 1, 380));
            this.mainMenu.Add(new Menu("豚丼", 1, 1, 1, 350));
            this.mainMenu.Add(new Menu("鮭定食", 1, 1, 0, 450));
            this.sideMenu1.Add(new Menu("野菜サラダ", 2, 1, 2, 100));
            this.sideMenu1.Add(new Menu("ポテトサラダ", 2, 1, 2, 130));
            this.sideMenu1.Add(new Menu("漬物", 2, 1, 0, 100));
            this.sideMenu1.Add(new Menu("生卵", 2, 0, 0, 60));
            this.sideMenu1.Add(new Menu("温泉卵", 2, 0, 0, 70));
            this.sideMenu2.Add(new Menu("みそ汁", 3, 1, 0, 60));
            this.sideMenu2.Add(new Menu("豚汁", 3, 0, 0, 190));
            this.sideMenu2.Add(new Menu("スープ", 3, 1, 0, 200));
            this.optionMenu.Add(new Menu("並", 4, 0, 1, 0));
            this.optionMenu.Add(new Menu("大盛り", 4, 0, 1, 100));
            this.optionMenu.Add(new Menu("特盛り", 4, 0, 1, 200));
            this.optionMenu.Add(new Menu("胡麻ドレッシング", 4, 0, 2, 0));
            this.optionMenu.Add(new Menu("和風ドレッシング", 4, 0, 2, 0));
        }

        /// <summary>
        /// 券売機を使う
        /// </summary>
        public void UseMachine()
        {
            this.ChooseMenus();
            this.SettlePrice();
        }

        /// <summary>
        /// メニューを選ぶ
        /// </summary>
        public void ChooseMenus()
        {
            this.ChooseMainMenu();
            if (Monitor.AcceptsEndOfOrder())
            {
                return;
            }

            this.ChooseSideMenus(this.sideMenu1);
            if (Monitor.AcceptsEndOfOrder())
            {
                return;
            }

            this.ChooseSideMenus(this.sideMenu2);
        }

        private void ChooseMainMenu()
        {
            this.ChooseMenu(this.mainMenu);
        }

        private void ChooseSideMenus(Category category)
        {
            while (true)
            {
                if (Monitor.AcceptsSkipOrder(category))
                {
                    return;
                }

                this.ChooseMenu(category);
            }
        }

        private void ChooseMenu(Category category)
        {
            var chosenMenu = category.ChooseMenu();
            this.chosenMenus.Add(chosenMenu);

            var selectableOptions = this.optionMenu.Menus.Where(x => x.O == chosenMenu.O).ToList();

            // 対応するオプションがないメニューもあるため、選択可能なオプションの有無を確認する
            if (selectableOptions.Count > 0)
            {
                var optionCategory = new Category(4, "オプション", selectableOptions);
                var chosenOption = optionCategory.ChooseMenu();
                this.chosenMenus.Add(chosenOption);
            }
        }

        private void SettlePrice()
        {
            Monitor.DisplayChosenMenuNames(this.chosenMenus.Menus);
            Monitor.DisplayTotalPrice(this.chosenMenus.GetTotalPrice());
        }
    }
}